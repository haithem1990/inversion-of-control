package com.hch.ioc.test;

import com.hch.ioc.core.IocRunner;
import com.hch.ioc.core.registries.BeanRegistry;
import com.hch.ioc.test.beans.impl.Z;

public class TestRunner {
    public static void main(String[] args) {
        IocRunner.run();
        Z z = (Z) BeanRegistry.getInstance().getBean(Z.class);
        z.testCache();
    }
}