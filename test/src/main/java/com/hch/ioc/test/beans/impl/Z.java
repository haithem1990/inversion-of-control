package com.hch.ioc.test.beans.impl;

import com.hch.ioc.core.annotations.IocInject;
import com.hch.ioc.core.annotations.IocScan;
import com.hch.ioc.test.beans.I10;

@IocScan
public class Z {

    @IocInject
    private I10 i10;

    public Z() {
    }

    public void testCache(){
        i10.test1("n",1);
        i10.test2("m",1);
    }

    public I10 getI10() {
        return i10;
    }

    public void setI10(I10 i10) {
        this.i10 = i10;
    }
}
