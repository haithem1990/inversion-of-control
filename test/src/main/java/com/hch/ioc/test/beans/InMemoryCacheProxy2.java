package com.hch.ioc.test.beans;

import com.hch.ioc.core.annotations.IocScan;
import com.hch.ioc.core.exceptions.SimpleIocException;
import com.hch.ioc.core.proxies.CacheableProxyProvider;

import java.lang.reflect.Method;

@IocScan
public class InMemoryCacheProxy2 implements CacheableProxyProvider {

    @Override
    public void before(Object proxy, Method method, Object[] args) {
        System.out.println("InMemoryCacheProxy 2 : before invoke " + method.getName());
    }

    @Override
    public void after(Object proxy, Method method, Object[] args) {
        System.out.println("InMemoryCacheProxy 2 : after invoke " + method.getName());
    }

    @Override
    public void afterThrow(Object proxy, Method method, Object[] args, Exception e) {
        System.out.println("InMemoryCacheProxy 2 : exception while invoke " + method.getName());
        throw new SimpleIocException(e);
    }

    @Override
    public void doFinally(Object proxy, Method method, Object[] args) {
        System.out.println("InMemoryCacheProxy 2 : finally invoke " + method.getName());
    }
}
