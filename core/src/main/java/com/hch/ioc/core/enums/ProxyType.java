package com.hch.ioc.core.enums;

public enum ProxyType {
    CACHE,
    LOCAL_DB_TRANSACTIONAL
}
