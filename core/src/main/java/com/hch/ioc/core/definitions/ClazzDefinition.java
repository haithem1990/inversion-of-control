package com.hch.ioc.core.definitions;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;

public class ClazzDefinition {

    private Map<String, Annotation> annotationMap;

    public ClazzDefinition() {
        this.annotationMap = new HashMap<>();
    }

    public Map<String, Annotation> getAnnotationMap() {
        return annotationMap;
    }

    public void setAnnotationMap(Map<String, Annotation> annotationMap) {
        this.annotationMap = annotationMap;
    }
}
