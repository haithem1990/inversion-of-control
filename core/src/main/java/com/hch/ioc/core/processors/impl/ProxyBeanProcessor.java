package com.hch.ioc.core.processors.impl;

import com.hch.ioc.core.annotations.Proxy;
import com.hch.ioc.core.definitions.IocScanDefinition;
import com.hch.ioc.core.enums.ProxyType;
import com.hch.ioc.core.exceptions.SimpleIocException;
import com.hch.ioc.core.processors.BeanProcessor;
import com.hch.ioc.core.processors.context.BeanProcessContext;
import com.hch.ioc.core.proxies.CacheableProxyProvider;
import com.hch.ioc.core.proxies.DynamicInvocationHandler;
import com.hch.ioc.core.proxies.ProxyProvider;
import com.hch.ioc.core.registries.BeanRegistry;

public class ProxyBeanProcessor implements BeanProcessor {

    private static ProxyBeanProcessor proxyBeanProcessor;

    private ProxyBeanProcessor() {
    }

    public static ProxyBeanProcessor getInstance() {
        if (proxyBeanProcessor == null) {
            proxyBeanProcessor = new ProxyBeanProcessor();
        }
        return proxyBeanProcessor;
    }

    @Override
    public void process(BeanProcessContext beanProcessContext) {
        if (beanProcessContext.getIocScanDefinition().getProxied()) {
            try {
                Object proxy = java.lang.reflect.Proxy.newProxyInstance(
                        beanProcessContext.getIocScanDefinition().getTypes().get(1).getClassLoader(),
                        new Class[]{Class.forName(beanProcessContext.getIocScanDefinition().getTypes().get(1).getName())},
                        new DynamicInvocationHandler(beanProcessContext.getObject(), defineProxyProvider(beanProcessContext.getIocScanDefinition()))
                );
                beanProcessContext.setObject(proxy);
            } catch (ClassNotFoundException e) {
                throw new SimpleIocException(e);
            }
        }
    }

    private ProxyProvider defineProxyProvider(IocScanDefinition iocScanDefinition) {
        if (((Proxy) iocScanDefinition.getClazzDefinition().getAnnotationMap().get("proxyDefinition")).type() == ProxyType.CACHE)
            return (ProxyProvider) BeanRegistry.getInstance().getBean(CacheableProxyProvider.class);
        else throw new SimpleIocException("no proxy provider found");
    }
}
