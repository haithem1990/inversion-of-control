package com.hch.ioc.core.definitions;

import com.hch.ioc.core.enums.Scope;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class IocScanDefinition {

    private Class<?> clazz;
    private List<Class<?>> types;
    private List<String> profiles;
    private Scope scope;
    private Boolean isProxied;
    private List<IocInjectDefinition> iocInjectDefinitions;
    private List<ExternalPropertyDefinition> externalPropertyDefinitions;
    private Map<String, MethodDefinition> methodDefinitionMap;
    private ClazzDefinition clazzDefinition;

    public IocScanDefinition(Class<?> clazz) {
        this.clazz = clazz;
        this.types = new LinkedList<>();
        types.add(clazz);
        this.isProxied = Boolean.FALSE;
        this.profiles = new LinkedList<>();
        this.iocInjectDefinitions = new LinkedList<>();
        this.externalPropertyDefinitions = new LinkedList<>();
        this.methodDefinitionMap = new HashMap<>();
        this.clazzDefinition = new ClazzDefinition();
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public void setClazz(Class<?> clazz) {
        this.clazz = clazz;
    }

    public List<Class<?>> getTypes() {
        return types;
    }

    public void setTypes(List<Class<?>> types) {
        this.types = types;
    }

    public List<IocInjectDefinition> getIocInjectDefinitions() {
        return iocInjectDefinitions;
    }

    public void setIocInjectDefinitions(List<IocInjectDefinition> iocInjectDefinitions) {
        this.iocInjectDefinitions = iocInjectDefinitions;
    }

    public List<ExternalPropertyDefinition> getExternalPropertyDefinitions() {
        return externalPropertyDefinitions;
    }

    public void setExternalPropertyDefinitions(List<ExternalPropertyDefinition> externalPropertyDefinitions) {
        this.externalPropertyDefinitions = externalPropertyDefinitions;
    }

    public List<String> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<String> profiles) {
        this.profiles = profiles;
    }

    public Scope getScope() {
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public Boolean getProxied() {
        return isProxied;
    }

    public void setProxied(Boolean proxied) {
        isProxied = proxied;
    }

    public Map<String, MethodDefinition> getMethodDefinitionMap() {
        return methodDefinitionMap;
    }

    public void setMethodDefinitionMap(Map<String, MethodDefinition> methodDefinitionMap) {
        this.methodDefinitionMap = methodDefinitionMap;
    }

    public ClazzDefinition getClazzDefinition() {
        return clazzDefinition;
    }

    public void setClazzDefinition(ClazzDefinition clazzDefinition) {
        this.clazzDefinition = clazzDefinition;
    }
}
